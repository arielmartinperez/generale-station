# Générale Station

<strong>[EN]</strong>

Générale Station is a curated and revisited version of the Générale Mono typeface, which was first released in 2017.

You can use for branding your next space station, to decorate the sides of your sea transport containers and (if our global civilization is reduced to crumbles) to write dystopic messages. Its modular, monospace nature allows to easily combine messages of varying length. It also has a variable version and extended language support.

Générale Station is an original typeface designed by [Ariel Martín Pérez](http://arielgraphisme.com/) and released by [Tunera Type Foundry](http://www.tunera.xyz/fonts/generale-station/). To discover how to use this font, please read the [FAQ](http://www.tunera.xyz/f.a.q/) 

<strong>[FR]</strong>

Générale station est une version épurée et revisitée de la fonte Générale Mono, qui avait été publiée pour la première fois 2017.

Vous pouvez l’utiliser pour faire l’identité visuelle de votre prochaine station spatiale, pour décorer les côtés de vos containers de transport maritimes, ou (si notre civilisation globale est réduite en miettes) pour écrire des messages dystopiques. Son caractère modulaire et de chasse fixe permet de combiner facilement des caractères des différentes largeurs. Il présente aussi une version variable et un support linguistique étendu.

Générale Station est une fonte typographique originale dessinée par [Ariel Martín Pérez](http://arielgraphisme.com/) et publiée par [Tunera Type Foundry](http://www.tunera.xyz/fr/fonts/generale-station/). Pour savoir comment utiliser cette fonte typographique, veuillez lire la [FAQ](http://www.tunera.xyz/fr/f.a.q/)

<strong>[ES]</strong>

Générale Station es una versión reducida y revisitada de del tipo de letra Générale Mono, que había sido publicado por primera vez en 2017.

Puedes utilizarlo para marcar su próxima estación espacial, para decorar los lados de tus contenedores de transporte marítimo y (si nuestra civilización global sufre un colapso) para escribir mensajes distópicos. Su carácter monoespacial modular permite combinar fácilmente letras de diferentes longitudes. También presenta una version variable y un amplio soporte lingüístico.

Générale Station es un tipo de letra original diseñado por [Ariel Martín Pérez](http://arielgraphisme.com/) y publicado por [Tunera Type Foundry](http://www.tunera.xyz/sp/fonts/generale-station/). Para saber cómo usar este tipo de letra, lea las [preguntas frecuentes](http://www.tunera.xyz/sp/f.a.q/)

## Specimen

![specimen1](documentation/specimen/generalestation-specimen-02.png)

![specimen2](documentation/specimen/generalestation-specimen-03.png)

![specimen3](documentation/specimen/generalestation-specimen-04.png)

![specimen4](documentation/specimen/generalestation-specimen-05.png)

## License

Générale Station is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
